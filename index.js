process.env["NTBA_FIX_319"] = 1;

const TelegramBot = require('node-telegram-bot-api');

var emoji = require('node-emoji')
var fs = require('fs');

var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '1',
  database : 'eduman'
});
connection.connect();

// replace the value below with the Telegram token you receive from @BotFather
const TOKEN = '1178670690:AAFk1frOpgsIEEFa-mBR4Sh8T-VGZkSu-sc';
var BOT = [];
var EDU_CENTER_NAME = 'Eduman - Center';
var EDU_CENTER_PHONE = '+998 (94) 263 85-23';
var BOT_NAME = 'Eduman - bot';
var BOT_CHANNEL = 'Eduman - channel';
// true

fs.readFile('settings.json',  'utf8', (err, data) => {
  console.log(data);
  BOT.SETTINGS = JSON.parse(data);
  EDU_CENTER_NAME = BOT.SETTINGS.center_name;
  EDU_CENTER_PHONE = BOT.SETTINGS.phone;
  BOT_NAME = BOT.SETTINGS.bot_name;
  BOT_CHANNEL = BOT.SETTINGS.center_channel;
  

});

console.log(BOT_CHANNEL);

// Create a bot that uses 'polling' to fetch new updates
const bot = new TelegramBot(TOKEN, {polling: true});

// // Matches "/echo [whatever]"
// bot.onText(/\/echo (.+)/, (msg, match) => {
//   // 'msg' is the received Message from Telegram
//   // 'match' is the result of executing the regexp above on the text content
//   // of the message
//   switch (key) {
//     case value:
      
//       break;
  
//     default:
//       break;
//   }

//   const chatId = msg.chat.id;
//   const resp = match[1]; // the captured "whatever"

//   // send back the matched "whatever" to the chat
//   bot.sendMessage(chatId, resp);
// });

// Listen for any kind of message. There are different kinds of
// messages.
bot.on('message', (msg) => {

  console.log(msg);
  const chatId = msg.chat.id;
  console.log(chatId);
  console.log(msg.text ? msg.text : null);
    // step 1
  if (msg.text === '/start') {

    let welcome_t = `Assalomu alaykum. <b>${BOT_NAME}</b> ga xush kelibsiz! Ushbu bot <b>${EDU_CENTER_NAME}</b> o'quv markazining o'quvchilari uchun tayyorlangan. 
    Sizning Telegram raqamingiz O'quv markaz bazasiga kiritilgan bo'lsa, Shu raqamni yuborishni bosing yoki maxsus siz uchun berilgan ACCESS_CODE ni yuboring.
    `;

    const welcome_k = {
        reply_markup: JSON.stringify({
            keyboard: [
                [
                    {
                        text: `Telefonimni jo'natish`,
                        request_contact: true          
                    }
                    
                ],
                
            ],
            one_time_keyboard: true,
            selective: true,
            resize_keyboard: true
        }),
        parse_mode:"HTML"
    }
    
    bot.sendMessage(chatId, welcome_t, welcome_k);
    return;
  }
  


  // step 2
  bot.on('contact', (msg) => {
    

    const main_k = {
      reply_markup: JSON.stringify({
          keyboard: [
              [
                { text: `Fanlar` },
                { text: `O'qituvchilar` }
              ],
              [
                {text: `Haqida`},
                {text: `Markaz manzili`}  
              ],
          ],
          one_time_keyboard: true,
          selective: true,
          resize_keyboard: true
      }),
      parse_mode:"HTML"
    }

    const chatId = msg.from.id;
    // console.log(msg.contact.phone_number.length);
    const number = msg.contact.phone_number.trimBy("+");
    console.trace(number);
    // emoji.find('')
    if (number.length === 12) {
      // console.log(11515);
      let sql1 = 'SELECT * from users_of_bot WHERE phone_number = ' + number;
      console.trace(sql1);
        
      connection.query(sql1, function (error, results, fields) {
          if (error) throw error;
          // console.log(results);
          
          if (results.length === 1 && results[0].status !== '0') {

            let sql2 = `UPDATE users_of_bot SET user_id = ${chatId} WHERE phone_number = ${number}`;
                        
            // console.log(sql2);
            connection.query(sql2, function (error, results, fields) {
              if (error) throw error;
              // console.log(results);
              
              // console.log(emoji.find('smartphone'));
              // bot.sendMessage(chatId, emoji.find('phone').emoji + 'Raqamingiz saqlandi.');
            });
            let text = `Sizga ruxsat berilgan ekan! <b>${EDU_CENTER_NAME}</b> botidan foydalanishiz mumkin. Eslatib o'tamiz: darslardagi bor-yo'qlama hisoboti ota-onangizni raqamiga haftalik borib turadi.
              Batafsil ma'lumotlarni bot <b>Haqida</b> tugmasini bosib bilib olasiz.
                        `;
            const extraText = `
            Sizning Ma'lumotlaringiz:
            ID: ${results[0].id}
            Ismingiz: ${results[0].first_name.toUpperCase()}
            Familiyangiz: ${results[0].last_name.toUpperCase()}
            Taqamingiz: ${results[0].phone_number}`;
            
            bot.sendMessage(chatId,  text + extraText, main_k);
            
          }else{

            sql1 = `SELECT * from users_of_bot WHERE user_id = ${chatId} `;
            console.log(sql1);
              
            connection.query(sql1, function (error, result, fields) {
              console.trace(result);
              // console.trace(fields);
              console.trace(fields);
              if (error) throw error;
              
            })
            

            let sql = `INSERT INTO users_of_bot (user_id, first_name, last_name, phone_number) VALUES ( ${msg.from.id}, '${msg.from.first_name}', '${msg.from.last_name}', ${number})`;
            // console.log(sql);
            
            connection.query(sql, function (err, res) {
              let text = `
              Raqamingizni saqlab oldik! O'quv markazimizga qiziqish bildirayotganizdan xursandmiz! Siz bilan imkon bo'lishi bilan aloqaga chiqamiz. Qo'ng'irog'imizni kuting.
              `;
              // console.trace(err);
              // console.trace(res);
              
              if (err) {
                console.trace(err.code);
                const phoneIcon = emoji.find('telephone_receiver').emoji;
                // console.log(err);
                if (err.code == 'ER_DUP_ENTRY') {
                  text = `
                  Siz bu akkauntiz bizning ba'zada mavjud ekan! Agar telefon raqamni yaqinda olgan bo'lsangiz, o'quv markaz bilan bog'laning(${phoneIcon}: ${EDU_CENTER_PHONE}) yoki sizga aloqaga chiqishimizni kuting.
                  Siz bilan imkon bo'lishi bilan aloqaga chiqamiz.
                              `   
                }else{
                  text = 'Xatolik kodi: ' + err.code;
                }
              }

              // console.trace(res);
              
              bot.sendMessage(chatId,  text, {parse_mode:"HTML"});
            });
            console.log('there is no such user or multiple users');
          }
      }); 

        
      }
      // connection.end();
      // send a message to the chat acknowledging receipt of their message
      return;
    });
      
  

  // console.log(typeof msg.text);
  
  // accepting haqida
  if (msg.text !== undefined && msg.text.toLowerCase() ===  'haqida') {
    // const message = parseInt(msg.text);

    bot.sendMessage(chatId, `<u> ${EDU_CENTER_NAME}</u> o'quv markazi haqida qisqacha ma'lumot. 
    O'quv markazimizda <b>6</b> ta yo'nalishda <b>10</b> dan ortiq kasbini <b>sevuvchi professional</b> o'qituvchilar dars o'tishadi. 
    O'quv markazda biz o'quvchilar va o'qituvchilar uchun qulay sharoit va doimiy e'tibor beramiz.
    O'quv markazimiz 2015 yildan beri o'z faoliyatini yuritib  kelmoqda, va shu paytgacha <b>1000</b> ga yaqin abiturientning universitet va institut kabi oliygohlarga kirishiga o'z hissasini qo'shdi. 
    `, {parse_mode: 'HTML'} );
    
    return;
  }
  
  // accepting ACCESS_CODE
  if (msg.text !== undefined && msg.text.toLowerCase() ===  'haqida') {
    // const message = parseInt(msg.text);

    bot.sendMessage(chatId, `<u> ${EDU_CENTER_NAME}</u> o'quv markazi haqida qisqacha ma'lumot. 
    O'quv markazimizda <b>6</b> ta yo'nalishda <b>10</b> dan ortiq kasbini <b>sevuvchi professional</b> o'qituvchilar dars o'tishadi. 
    O'quv markazda biz o'quvchilar va o'qituvchilar uchun qulay sharoit va doimiy e'tibor beramiz.
    O'quv markazimiz 2015 yildan beri o'z faoliyatini yuritib  kelmoqda, va shu paytgacha <b>1000</b> ga yaqin abiturientning universitet va institut kabi oliygohlarga kirishiga o'z hissasini qo'shdi. 
    `, {parse_mode: 'HTML'} );
    
    return;
  }

  // accepting ACCESS_CODE
  if (msg.text !==  undefined && msg.text.length === 6) {
    const message = parseInt(msg.text);
    connection.query('SELECT * from users_of_bot WHERE access_code = ' + message, function (error, results, fields) {
        if (error) throw error;
        console.log(results);

        if (results.length === 1) {
          
            // let sql = `INSERT INTO users_of_bot (user_id, first_name, last_name) VALUES ( ${msg.from.id}, '${msg.from.first_name}', '${msg.from.last_name}')`;

            let sql = `UPDATE users_of_bot SET user_id = ${chatId} WHERE id = ` + results[0].id;
             
            connection.query(sql, function (err, res) {
              if (err) {
                console.log(err);
              }
              console.log(res);
              bot.sendMessage(chatId, `Siz kiritgan kodiz to'g'ri ekan. <b>${EDU_CENTER_NAME}</b> botidan foydalanishiz uchun siz telefon raqamizni jo'natishiz kerak.`,
              {parse_mode: "HTML"} );
            });

        }else{
          
          bot.sendMessage(chatId, `Sizga ruxsat berilmagan ekan! <b>${EDU_CENTER_NAME}</b> botidan foydalanishiz uchun oldin o'quv markaz qabulxonasidan ro'yxatdan o'ting.`, {parse_mode: "HTML"});
        }
        console.log(results);
    });
    // connection.end();   
    return;
  }
  


  
  // send a message to the chat acknowledging receipt of their message
  // bot.sendMessage(chatId, 'Received your message');
});


bot.on("polling_error", (err) => console.log(err));


String.prototype.trimBy = function (chars) {
  if (chars === undefined) {
    chars = "\s";
  }
  return this.replace(new RegExp("^[" + chars + "]"), "")
}